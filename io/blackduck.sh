#!/bin/bash

#Download Synopsys Detect
wget -O detect.sh https://detect.synopsys.com/detect.sh

#Update to be executable
chmod a+x detect.sh

#Run Detect against configured BlackDuck instance
./detect.sh --blackduck.url='https://poc88.blackduck.synopsys.com/' --blackduck.api.token='API-Token' --detect.project.name='vulnado-gitlab' --detect.project.version.name='0.0.1-SNAPSHOT' --detect.force.success=true
